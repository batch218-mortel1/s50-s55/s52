import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard'

export default function Courses() {

	console.log(coursesData);
	console.log(coursesData[0]);

	// The "map" method loops through the individual course objects in our array and return a component for each course
	// Everytime the map method loops through the data, it creates a "CourseCard" component and then passes the current element in our couseData array using the "courseProp"
	
	const courses = coursesData.map(course => {
		return (
			<CourseCard key={course.id} course={course}/>
		);
	})

	// Props Drilling - allows us to pass information from one component to another using "props"
	// Curly braces {} are used for props to signify that we are providing/passing information
	
	return(
		<>
		{courses}
		</>
	)
}