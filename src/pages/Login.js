import {useState, useEffect} from 'react';
import { Form, Button } from 'react-bootstrap';

export default function Login() {

    // State hooks to store the values of the input fields
    const [email, setEmail] = useState("");
    const [password, setPassword] =  useState("");
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

    console.log(email);
    console.log(password);


    // Function to simulate redirection via form submission
    function logIn(e) {
        // prevents page redirection via form submission                                    
        e.preventDefault()

        // Clear input fields
        setEmail("");
        setPassword("");

        alert('Thank you for registering!');

    }

    useEffect(() => {

    /*
    MINI ACTIVITY
    Create a validation to enable the submit button when all fields are populated and both passwords match.
    */

    // Validation to enable the submit button when all fields are populated and both passwords match.
    if((email !== '' && password !== '')){
        setIsActive(true);
        } else {
            setIsActive(false);
        }
        

    }, [email, password])

    return (
        <Form onSubmit={(e) => logIn(e)}>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
	                type="email" 
	                placeholder="Enter email" 
                    value={email}
                    onChange ={e => setEmail(e.target.value)}
	                required
                />
              
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Password" 
                    value={password}
                    onChange ={e => setPassword(e.target.value)}
	                required
                />
            </Form.Group>

                { isActive ? 

                <Button variant="primary" type="submit" id="submitBtn">
                    Submit
                </Button>
                :
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                Submit
                </Button>
                }
            
            
        </Form>
    )

}